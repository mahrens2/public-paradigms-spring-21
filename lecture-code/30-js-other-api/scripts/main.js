console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered get form info');
    var name = document.getElementById("name-text").value;
    makeNetworkCallToAgeApi(name);
} // end of getFormInfo

function makeNetworkCallToAgeApi(name){
    console.log('entered makeNetworkCallToAgeApi');
    var url = "https://api.agify.io/?name=" + name;
    var xhr = new XMLHttpRequest(); // 1. creating req
    xhr.open("GET", url, true); // 2. configure request attributes

    // set up onload - triggered when nw response is received
    // must be defined before making the network call
    xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
        // do something
        updateAgeWithResponse(name, xhr.responseText);
    } // end of onload

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    } // end of onerror

    xhr.send(null); // actually send req with no message body
} // end of makeNetworkCallToAgeApi

function updateAgeWithResponse(name, response_text){
    // extract json info from response
    var response_json = JSON.parse(response_text);
    // update label with it
    var label1 = document.getElementById('response-line1');

    if(response_json['age'] == null){
        label1.innerHTML = 'Apologies, we could not find your name.';
    } else{
        label1.innerHTML = name + ', your age is ' + response_json['age'];
        // make nw call to number api
        var age = parseInt(response_json['age']);
        makeNetworkCallToNumbersApi(age);
    }

} // end of updateAgeWithResponse

function makeNetworkCallToNumbersApi(age){
    console.log('enetered makeNetworkCallToNumbersApi');
    var url = "http://numbersapi.com/" + age;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e) {
        console.log('got nw response from numbersapi');
        updateTriviaWithResponse(age, xhr.responseText);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null); // send request without bosy
} // end of makeNetworkCallToNumbersApi

function updateTriviaWithResponse(age, response_text){
    var label2 = document.getElementById("response-line2");
    label2.innerHTML = response_text;

    // dynamically adding a label
    label_item = document.createElement("label"); // weird - "label" is a class name
    label_item.setAttribute("id", "dynamic-label" ); // setAttribute(property_name, value) so here id is property name of button object

    var item_text = document.createTextNode(response_text);
    label_item.appendChild(item_text);

    // option 1: directly add to document
    // adding label to document
    //document.body.appendChild(label_item);

    // option 2:
    // adding label as sibling to paragraphs
    var response_div = document.getElementById("response-div");
    response_div.appendChild(label_item);

} // end of updateTriviaWithResponse
